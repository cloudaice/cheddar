package main

/*
cheddargo-sandbox.go is a sandbox file for testing Go code, or
writing it down from tutorials to better understand the
langage. I can't guarantee everything written in the file
will be compilable, let alone runnable. If so, it may be 
spun off into it's file.
*/

import (
	"fmt"
	"os"
)

// Default state
/*
func main() {
	fmt.Println("Hello, Squeaks!")
}
*/

// Effective Go and beyond
var {
	HOME = os.Getenv("HOME")
	USER = os.Getenv("USER")
	GOROOT = os.Getenv("GOROOT")
}

func main(){
	x := []int{1,2,3}
	y := []int{4,5,6}
	x = append(x, y...)
	fmt.Println(x)
}
// Examples end here
